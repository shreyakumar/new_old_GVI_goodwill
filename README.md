# New Old GVI Goodwill


GVI Project Handover (Spring 2020)

# Access Details:

Code repositories: 
- GVI visualization 
-- current: https://github.com/HL3rd/gviwebapp
-- old: https://github.com/TT-Zop/gviVisualization
- GVI field notes 
-- current: https://github.com/aslavin/GVI_App - moved to here

See email and account information details in https://docs.google.com/document/d/1jIzASQhfOklBnKSmf-W0iqVzHngL9ZeRBXxOSbnWx1w/edit 


# Accounts:

Plotly:

email = 

username = see google doc

password = see google doc


Goodwill GVI Sharepoint:

url = https://goodwillni.sharepoint.com

the old sharepoint does not work anymore

username = see google doc

password = see google doc

Architectural Overview:


# Technologies:

Jupyter Notebook
Environment utilized to interactively run Python files. We used a Jupyter notebook to pull data from the GVI site, parse the data, and create Plotly graphs with the data.
Plotly
Python-friendly graphing software. We used Plotly to create our visuals (which were then exported as iframes).
Flask
Python framework for running a web application. We used Flask to deploy the new Goodwill site. The graphics generated with Plotly are embedded as iframes within the home HTML page.
HTML
Backbone of our GVI website. The HTML files display the iframes and render necessary elements on the webpage (e.g. navigation bar, section headers).
CSS
Used to style HTML pages (e.g. alter colors, fonts, alignment). 
Bootstrap
CSS framework that abstracts a lot of the element creation/formatting. We used this to create our navbar and video carousel.

Knowledge Details:

# Tutorials:


- Flask:
Quickstart
https://flask.palletsprojects.com/en/1.1.x/quickstart/
https://realpython.com/introduction-to-flask-part-1-setting-up-a-static-site/
- Adding CSS & JavaScript
https://exploreflask.com/en/latest/static.html
- Sharepoint API: https://readthedocs.org/projects/shareplum/downloads/pdf/latest/
- Plotly (for Python): https://plotly.com/python/
- Bootstrap Examples: https://startbootstrap.com/snippets/

# State of the Project/Client Details:

The GVI team asked for 2 things: visualizations of their data and an easier way to record information about their contacts. This document focuses on the former request because that is what we primarily worked on in the Spring 2020 semester. Often, GVI receives calls and requests for statistics about their program. Instead of having to handle all of these requests individually, GVI would like to have a central place that people can go to to get this information.We have created a website to display GVI’s data as visualizations/charts. So far, we have created visualizations to display data concerning the gender of GVI’s contacts, how often contacts are made, and the age of GVI’s contacts. In the past, they have asked for data visualizations of contacts by zip code, ethnicity, gender, age group, risk, gang, and month of first contact. The next steps would be to create the remaining visualizations as well as create a hosting option. In our most recent meeting with Isaac Hunt, SBGVI & Gary for Life Supervisor, he mentioned that he would like to include more information about the community service work (e.g. Goodwill’s COVID-19 response) that his team does. However, before we can work on that, we will need access to that data. Isaac also sent us the format for his GVI monthly report and mentioned that he would like some of these metrics added to the website. 

# Contacts:

see google doc



Usually, we contact Isaac a few times during the semester, although communication with the GVI team has been spotty at times. They are a very busy team doing important work with a few dedicated, hardworking members. 




# Deployment Details:

Currently, our website is hosted locally. We need to choose which hosting method we would like to use and change our Flask app to be a non-development version. Our tentative plan is to use Goodwill resources to deploy and host the website once it is complete. 


Area Experts:
see google doc - 






